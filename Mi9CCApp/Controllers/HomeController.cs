﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Mi9CCApp.Models;

namespace Mi9CCApp.Controllers
{
    public class HomeController : ApiController
    {

        //public ActionResult Index()
        //{
        //    return View();
        //}

        [System.Web.Mvc.HttpPost]
        public HttpResponseMessage Index(DataRequest model)
        {
            HttpResponseMessage response;

            if (!this.ModelState.IsValid)
            {
                // Return BadRequest if ModelState is not valid.
                response = Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Could not decode request: JSON parsing failed" });
            }
            else
            {
                //process data
                try
                {
                    var showList = (List<Show>)model.PayLoad;

                    // Find all shows matching conditions
                    var shows = (from show in showList
                                 where show.Drm && show.EpisodeCount > 0
                                 select new Response() { image = show.Image.showImage, slug = show.Slug, title = show.Title }).ToList();

                    // Return OK with json response
                    response = Request.CreateResponse(HttpStatusCode.OK, new { response = shows });
                }
                catch
                {
                    // Return BadRequest if any exception thrown
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Could not decode request: JSON parsing failed" });
                }
            }

            return response;
        }

    }
}
