﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mi9CCApp.Models
{
    public class DataRequest
    {
        public IList<Show> PayLoad { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
        public int TotalRecords { get; set; }
    }
}