﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Hosting;
using System.Web.Http.Routing;
using Mi9CCApp.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Mi9CCApp.Tests.Controllers
{
    [TestClass]
    public class HomeController
    {
        [TestMethod]
        public void GetShows_ReturnOK_WithValidDataRequest()
        {
            // Valid testing data
            var shows = new Mi9CCApp.Models.DataRequest()
            {
                PayLoad = new List<Show> {
                    new Show()
                        {
                            Country = "TextCountry",
                            Description = "TextDescription",
                            Drm = true,
                            EpisodeCount = 0,
                            Genre = "TextGenre",
                            Image = new Mi9CCApp.Models.Image(){showImage = "http://catchup.ninemsn.com.au/img/jump-in/shows/TheTaste1280.jpg"},
                            Language = "TextLanguage",
                            NextEpisode = new NextEpisode(),
                            PrimaryColour = "TextPrimaryColour",
                            Seasons = new List<Season>(),
                            Slug = "TextSlug",
                            Title = "TextTitle",
                            TvChannel = "TextTvChannel"

                        }  },
                Skip = 0,
                Take = 0,
                TotalRecords = 0
            };

            var config = new HttpConfiguration();
            var request = new HttpRequestMessage(HttpMethod.Post, "http://localhost/");
            var route = config.Routes.MapHttpRoute("DefaultApi", "");
            var routeData = new HttpRouteData(route, new HttpRouteValueDictionary { { "controller", "Home" } });
            var controller = new Mi9CCApp.Controllers.HomeController();
            controller.ControllerContext = new HttpControllerContext(config, routeData, request);
            controller.Request = request;
            controller.Request.Properties[HttpPropertyKeys.HttpConfigurationKey] = config;

            // Act
            var result = controller.Index(shows);

            // Assert
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }


        [TestMethod]
        public void GetShows_ReturnBadRequest_WithInvalidRequestData()
        {
            // Invalid testing data
            var shows = new Mi9CCApp.Models.DataRequest()
            {
                PayLoad = null,
                Skip = 0,
                Take = 0,
                TotalRecords = 0
            };

            var config = new HttpConfiguration();
            var request = new HttpRequestMessage(HttpMethod.Post, "http://localhost/");
            var route = config.Routes.MapHttpRoute("DefaultApi", "");
            var routeData = new HttpRouteData(route, new HttpRouteValueDictionary { { "controller", "Home" } });
            var controller = new Mi9CCApp.Controllers.HomeController();
            controller.ControllerContext = new HttpControllerContext(config, routeData, request);
            controller.Request = request;
            controller.Request.Properties[HttpPropertyKeys.HttpConfigurationKey] = config;

            // Act
            var result = controller.Index(shows);

            // Assert
            Assert.AreEqual(HttpStatusCode.BadRequest, result.StatusCode);
        }
    }
}
